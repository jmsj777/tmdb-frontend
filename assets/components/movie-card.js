import { img_base, base_url, api_key } from "../core/secrets.js"
import MoviePage from "./movie-page.js"

const linkElem = document.createElement("link")
linkElem.setAttribute("rel", "stylesheet")
linkElem.setAttribute("href", "assets/css/movie-card.css")

const template = document.createElement("template")
template.innerHTML = `
  <div class="movie-card"> 
    <img id="movies-poster-path" />
    <div class="text">
      <h2> 
        <t id="movies-title">No Title</t>
      </h2>
      <h3>
        Release date: <br/>
        <t id="movies-release-date">00-00-0000</t>
      </h3>
      <h4 >
        Popularity:
        <t id="movies-popularity">0.00</t>
      </h4>
    </div>
  </div>
`

class MovieCard extends HTMLElement {
  constructor(id, title, releaseDate, popularity, posterPath) {
    super()
    this.shadow = this.attachShadow({ mode: "open" })
    this.movie_id = id
    this.title = title
    this.releaseDate = releaseDate
    this.popularity = popularity
    this.posterPath = posterPath
  }

  connectedCallback() {
    this.render()
    this.shadow.children[1].addEventListener("click", () => this.gotoMovie())
  }

  disconnectedCallback() {
    this.shadow.children[1].removeEventListener()
  }

  render() {
    this.shadow.appendChild(linkElem.cloneNode(true))
    this.shadow.appendChild(template.content.cloneNode(true))

    this.shadow.getElementById("movies-title").innerText = this.title
    this.shadow.getElementById(
      "movies-release-date"
    ).innerText = this.releaseDate
    this.shadow.getElementById("movies-popularity").innerText = this.popularity
    this.shadow.getElementById(
      "movies-poster-path"
    ).src = `${img_base}/w500${this.posterPath}`
  }

  async gotoMovie() {
    const main = document.getElementById("main")
    main.setAttribute("hidden", "")

    let res = await fetch(
      `${base_url}/movie/${this.movie_id}?api_key=${api_key}`
    )
    let movie = await res.json()
    // main.innerHTML = ""
    // console.log(json)
    let moviePage = new MoviePage(
      movie.id,
      movie.title,
      movie.release_date,
      movie.popularity,
      movie.poster_path
    )

    document.body.appendChild(moviePage)
  }
}

customElements.define("movie-card", MovieCard)

export default MovieCard
