# TMDB Frontend

A frontend to access the movie database API (https://developers.themoviedb.org/3)
The work is mentored by https://github.com/heliohachimine

This website allows you to:

- Browse movies from the database (Popular, Latest, Top Rated and Upcoming)
- See the details of each movie

## Technologies used

- Prototype made with Figma
- Front-end in vanilla javascript.

## Screenshots

Here you can see a list of movies found in a search
![](https://i.imgur.com/h9eXdcK.png)

## Setup:

Launch front/index.html with a live server in the port 5500 (the origin is configured to be http://127.0.0.1:5500 for the server to work), and it is all set!
